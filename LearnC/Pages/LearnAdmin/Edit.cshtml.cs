using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LearnC.Data;
using LearnC.Models;

namespace LearnC.Pages.LearnAdmin
{
    public class EditModel : PageModel
    {
        private readonly LearnC.Data.LearnCContext _context;

        public EditModel(LearnC.Data.LearnCContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Learn Learn { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Learn = await _context.Learn.FirstOrDefaultAsync(m => m.LearnID == id);

            if (Learn == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Learn).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LearnExists(Learn.LearnID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool LearnExists(int id)
        {
            return _context.Learn.Any(e => e.LearnID == id);
        }
    }
}
