using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using LearnC.Data;
using LearnC.Models;

namespace LearnC.Pages.LearnAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly LearnC.Data.LearnCContext _context;

        public DetailsModel(LearnC.Data.LearnCContext context)
        {
            _context = context;
        }

        public Learn Learn { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Learn = await _context.Learn.FirstOrDefaultAsync(m => m.LearnID == id);

            if (Learn == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
